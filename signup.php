
<?php
include_once "config/userConfig.php";

include_once "models/classUser.php";
$user = new User($db);

/* Signup Form */
//var_dump($_POST['signupSubmit']);
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
  //die('dominicvcvcxvcxv');
  $username=$_POST['InputName'];
  $email=$_POST['InputEmail1'];
  $password=$_POST['InputPassword1'];
  /* Regular expression check */
  $username_check = preg_match('~^[A-Za-z0-9_]{3,20}$~i', $username);
  $email_check = preg_match('~^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$~i', $email);
  $password_check = preg_match('~^[A-Za-z0-9!@#$%^&*()_]{6,20}$~i', $password);

  if($username_check && $email_check && $password_check)
  {
    $uid=$user->register($username, $email, $password);
    if($uid)
    {
      $url='index.php';
      header("Location: $url"); // Page redirecting to home.php 
    }
    else
    {
      $errorMsgReg="Username or Email already exists.";
    }
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>MovieInfo</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/cyborg/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Login / Sign Up</a>
        </div>
      </div>
    </nav>

    <div class="container">
    <form method="POST">
  <fieldset>
     <div class="form-group">
      <label for="exampleInputEmail1">Username</label>
      <input type="text" name="InputName" class="form-control" id="InputName" aria-describedby="emailHelp" placeholder="Enter Username">
      <small id="emailHelp" class="form-text text-muted">Please enter a username</small>
    </div>
  
    <div class="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email" class="form-control" name="InputEmail1" id="InputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
      <label for="InputPassword1">Password</label>
      <input type="password" class="form-control" name="InputPassword1" id="InputPassword1" placeholder="Password">
    </div>
    <button type="submit" name="signupSubmit" class="btn btn-primary">Submit</button>
  </fieldset>
</form>
<span>Already have a User?</span>
<a href="login.php" class="btn btn-default">Login</a>
    </div>

    <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="js/main.js"></script>
  <script>
    getMovie();
  </script>
  </body>
</html>
