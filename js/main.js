$(document).ready(() => {
  $('#searchForm').on('submit', (e) => {
    let searchText = $('#searchText').val();
    getMovies(searchText);
    e.preventDefault();
  });
});

const apiKey = 'api_key=531eaffcac14a8c431f91d7a77a345e8';
const imagePath = ' http://image.tmdb.org/t/p/original';
function getMovies(searchText){
  axios.get('https://api.themoviedb.org/3/search/movie?'+ apiKey +'&query='+searchText)
    .then((response) => {
      console.log(response.data.results);
      let movies = response.data.results;
      let output = '';
      $.each(movies, (index, movie) => {
        output += `
          <div class="col-md-3">
            <div class="well text-center">
              <img src="${imagePath + movie.poster_path}">
              <h5>${movie.original_title}</h5>
              <a onclick="movieSelected('${movie.id}')" class="btn btn-primary" href="#">Movie Details</a>
            </div>
          </div>
        `;
      });

      $('#movies').html(output);
    })
    .catch((err) => {
      console.log(err);
    });
}

function movieSelected(id){
  sessionStorage.setItem('movieId', id);
  window.location = 'movie.php';
  return false;
}

function getMovie(){
  let movieId = sessionStorage.getItem('movieId');

  axios.get('https://api.themoviedb.org/3/movie/'+movieId+'?'+apiKey)
    .then((response) => {
      console.log(response);
      let movie = response.data;

      let output =`
        <div class="row">
          <div class="col-md-4">
            <img src="${imagePath + movie.poster_path}" class="thumbnail">
          </div>
          <div class="col-md-8">
            <h2>${movie.original_title}</h2>
            <ul class="list-group">
              <li class="list-group-item"><strong>Genre:</strong> ${movie.genres[(movie.genres).length-1].name}</li>
              <li class="list-group-item"><strong>Released:</strong> ${movie.release_date}</li>
              <li class="list-group-item"><strong>Rated:</strong> ${movie.vote_average}</li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="well">
            <h3>Plot</h3>
            ${movie.overview}
            <hr>
            <a href="index.html" class="btn btn-default">Go Back To Search</a>
          </div>
        </div>
      `;

      $('#movie').html(output);
    })
    .catch((err) => {
      console.log(err);
    });
}

function getMovieDetails(movieId){

  axios.get('https://api.themoviedb.org/3/movie/'+movieId+'?'+apiKey)
    .then((response) => {
      //console.log(response.data);
     
      let movies = response.data;
       let output = '';
      $.each(movies, (index, movie) => {
        output += `
          <div class="col-md-3">
            <div class="well text-center">
              <img src="${imagePath + movie.poster_path}">
              <h5>${movie.original_title}</h5>
              <a onclick="movieSelected('${movie.id}')" class="btn btn-primary" href="#">Movie Details</a>
            </div>
          </div>
        `;
      });

      $('#movies').html(output);
    })
    .catch((err) => {
      console.log(err);
    });
}

function getMovieList(){

  axios.get("retrieveMovies.php")({
    // userId : userId,
  })
    .then((response) => {
      //console.log(response.data[0].MovieID);
      console.log(response.data);
      let movies = response.data;
      movies.forEach((movie) => {
         console.log(movie.MovieID);
         getMovieDetails(movie.MovieID);
        
      });
    })
    .catch((err) => {
      console.log(err);
    });
}

function AddMovieToDatabase(){
  let movieId = sessionStorage.getItem('movieId');
  axios.post('postMovieToDB.php', {
    movieId : movieId,
    userId : userId,
  })
    .then((response) => {
      console.log(response);
      let movie = response.data;
    })
    .catch((err) => {
      console.log(err);
    });
}
