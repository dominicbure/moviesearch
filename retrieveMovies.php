<?php
// include database and object files
include_once '../config/database.php';
include_once '../models/retrieveWatchlist.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
 
// pass connection to objects
$watchList = new RetrieveWatchlist($db);
//var_dump($watchList->read());

$userId =$_SESSION["UserId"];
if(isset($userId)) {
	$watchList->read($userId);
}

