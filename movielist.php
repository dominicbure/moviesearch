<?php
include_once "config/userConfig.php";
include_once "models/classUser.php";

$user = new User($db);
if(!($user->is_loggedin()))
{
 $user->redirect('signup.php');
}
$userDetails=$user->userDetails($_SESSION['UserId']);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Watch List</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/cyborg/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Watch List</a>
           <?php include "userNavbar.php"; ?>
        </div>
      </div>
    </nav>

    <div class="container">
      <div id="movies" class="row"></div>
    </div>

    <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="js/main.js"></script>
  <script type="text/javascript" >
    getMovieList();
   getMovieDetails();
    var userId = '<?php echo $_SESSION["UserId"]; ?>';
    //console.log(userId);
  </script>
  </body>
</html>
