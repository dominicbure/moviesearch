<?php
include_once "config/userConfig.php";
include_once "models/classUser.php";

$user = new User($db);
$userDetails=$user->userDetails($_SESSION['UserId']);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>MovieInfo</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/cyborg/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">MovieInfo</a>
         <?php include "userNavbar.php"; ?>
          <?php if(($user->is_loggedin())) { ?>
        <a onclick="AddMovieToDatabase('${movie.id}')" class="btn btn-primary" href="#">Save to Watchlist</a>
        
        <?php } ?>
         <a class="btn btn-primary" href="movielist.php">View Watchlist</a>
        </div>
      </div>
    </nav>

    <div class="container">
      <div id="movie" class="well">

        
      </div>
    </div>

    <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="js/main.js"></script>
  <script>
    getMovie();
    var userId = '<?php echo $_SESSION["UserId"]; ?>';
    //console.log(userId);
  </script>
  </body>
</html>
