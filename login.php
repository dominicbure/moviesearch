
<?php
include_once "config/userConfig.php";

include_once "models/classUser.php";
$user = new User($db);

/* Signup Form */
//var_dump($_POST['signupSubmit']);
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
 
  $usernameEmail=$_POST['InputNameEmail'];
  $password=$_POST['InputPassword1'];
  if(strlen(trim($usernameEmail))>1 && strlen(trim($password))>1 )
  {
  //die('dominic');
  $uid=$user->login($usernameEmail,$password);
  if($uid)
  {

  $url='index.php';
  header("Location: $url"); // Page redirecting to home.php 
  }
  else
  {
  $errorMsgLogin="Please check login details.";
  }
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>MovieInfo</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/cyborg/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Login / Sign Up</a>
        </div>
      </div>
    </nav>

    <div class="container">
    <form method="POST">
  <fieldset>
     <div class="form-group">
      <label for="exampleInputEmail1">User name / email</label>
      <input type="text" name="InputNameEmail" class="form-control" id="InputName" aria-describedby="emailHelp" placeholder="Enter email">
      <small id="emailHelp" class="form-text text-muted">Please enter a username or email</small>
    </div>
  
    <div class="form-group">
      <label for="InputPassword1">Password</label>
      <input type="password" class="form-control" name="InputPassword1" id="InputPassword1" placeholder="Password">
    </div>
    <button type="submit" name="loginSubmit" class="btn btn-primary">Submit</button>
  </fieldset>
</form>
    </div>

    <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="js/main.js"></script>
  <script>
    getMovie();
  </script>
  </body>
</html>
