<?php
class User
{
  	 // database connection and table name
    private $conn;
    private $table_name = "Users";
 
    public function __construct($db){
        $this->conn = $db;
    }

 
    public function register($userName,$userEmail,$userPassword)
    {
       try
       {
          $query = "SELECT UserId FROM Users WHERE userName=? OR userEmail=?";
          $stmt = $this->conn->prepare($query);
          $stmt->execute([$userName, $userEmail]);
          $count=$stmt->rowCount();
          if($count<1) {
            $newPassword = hash('sha256', $userPassword);
            $query = "INSERT INTO " .$this->table_name . " (userName, Password, Email)  VALUES(?, ?, ?)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([$userName, $newPassword, $userEmail]);
            return $stmt;
          }
           
       }
       catch(PDOException $e)
       {
           echo $e->getMessage();
       }    
    }
 
    public function login($userNameEmail,$userPassword)
    {
       try
       {
          $hash_password= hash('sha256', $userPassword); //Password encryption
          $query ="SELECT UserId FROM " .$this->table_name.  " WHERE UserName=? OR Email=? AND password=? LIMIT 1";
          $stmt = $this->conn->prepare($query);
          $stmt->execute([$userNameEmail, $userNameEmail, $hash_password]);
          $count=$stmt->rowCount();
          $data=$stmt->fetch(PDO::FETCH_OBJ);
          $db = null;
          if($count)
          {
            $_SESSION['UserId']=$data->UserId; // Storing user session value
            return true;
          }
          else
          {
            return false;
          } 
       }
       catch(PDOException $e)
       {
           echo $e->getMessage();
       }
   }
 
   public function is_loggedin()
   {
      if(isset($_SESSION['UserId']))
      {
         return true;
      }
   }
 
   public function redirect($url)
   {
       header("Location: $url");
   }
 
   public function logout($url)
   {
        session_destroy();
        unset($_SESSION['UserId']);
        $this->redirect($url);
        return true;
   }

   /* User Details */
  public function userDetails($UserId)
  {
    try{
      
      $query = "SELECT Email, UserName FROM " .$this->table_name. " WHERE UserId=?";

      $stmt = $this->conn->prepare($query);

      $stmt->execute([$UserId]);
      
      $data = $stmt->fetch(PDO::FETCH_OBJ); //User data

      return $data;
    }
    catch(PDOException $e) {
      echo $e->getMessage();
    }
  }
}
?>
