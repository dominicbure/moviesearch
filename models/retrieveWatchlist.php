<?php
class RetrieveWatchlist{
 
    // database connection and table name
    private $conn;
    private $table_name = "Watchlist";
 
    public function __construct($db){
        $this->conn = $db;
    }

    // create entry
    function read($userId){
        //write query
        $query = "SELECT * FROM ". $this->table_name. " WHERE UserIdWatchlist =? ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$userId]);
        //$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $jsonresult = json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
        header('Content-Type: application/json');
       //echo '<pre>'; print_r($result);

        echo $jsonresult;
 
        
 
        if($stmt->execute()){
            return true;
        }else{
            echo 'failure';
            return false;
        }
 
    }
}
