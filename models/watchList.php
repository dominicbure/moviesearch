<?php
class WatchList{
 
    // database connection and table name
    private $conn;
    private $table_name = "Watchlist";
 
    // object properties
    public $movieID;
    public $timestamp;
 
    public function __construct($db){
        $this->conn = $db;
    }
 
    // create entry
    function create($userId, $movieID){
 
        // to get time-stamp for 'created' field
        $this->timestamp = date('Y-m-d H:i:s');

        //write query
        $query = "INSERT INTO " . $this->table_name ." (movieID, UserIdWatchlist, dateCreated) VALUES (?, (SELECT UserId from Users WHERE UserId=?), ?) ";
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$movieID, $userId, $timestamp]);
        return $stmt;
 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
 
    }
}
