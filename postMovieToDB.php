<?php
// include database and object files
include_once 'config/database.php';
include_once 'models/watchList.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
 
// pass connection to objects
$watchList = new WatchList($db);

$_POST = json_decode(file_get_contents("php://input"),true);
$movieID = $_POST['movieId'];
$userId = $_POST['userId'];

if(isset($movieID) && isset($userId)) {
	$watchList->create($userId, $movieID);
}

